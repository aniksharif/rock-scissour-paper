import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

const reply=''
const ques=''
export default class App extends React.Component {

constructor(props){
  super(props);
  this.state={
    answer:'',
    result:'',
    
  }
}

result(reply){
  console.log('reply:',reply)
  if(ques===reply){
    this.setState({answer:reply, result:ques===reply?'Draw':''})
  }
  else if(ques==='Rock' && reply==='Paper' )
  {
    this.setState({result:'Lost',answer:reply})
  }
  else if(ques==='Paper' && reply==='Scissor' )
  {
    this.setState({result:'Lost',answer:reply})
  }
  else if(ques==='Scissor' && reply==='Rock' )
  {
    this.setState({result:'Lost',answer:reply})
  }
  else{
    this.setState({result:'Win',answer:reply})
  }
}



tactic(){
  var ans=Math.random()*10;


if(ans>6){
  reply='Rock'
  
  
  this.result(reply)

}
else if(ans>3 && ans<6)
{
  reply='Paper'
  
 
  this.result(reply)
 
}
else{
  reply='Scissor'
  this.result(reply)
}



}

  render() {
    return (
      <View style={{flex:1}}>
      <View style={styles.container}>
        <View style={styles.left}>
          <TouchableOpacity onPress={()=>{ques='Rock';this.tactic();}}>
            <Text>Rock</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ques='Paper';this.tactic();}}>
            <Text>Paper</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ques='Scissor';this.tactic();}}>
            <Text>Scissor</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.right}>
          <Text>{this.state.answer}</Text>
        </View>



      </View>
      <View style={{marginHorizontal:100,marginBottom:100}}>
      <Text>{this.state.result}</Text>
    </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row"
  },
  left: {
    padding: 20
  },
  right: {
    padding: 20
  }
});
