import React, { Component } from 'react';
import {StyleSheet, ImageBackground, View, StatusBar, Button,} from 'react-native';

console.disableYellowBox = true;



export default class Home extends Component {
    componentDidMount() {
        StatusBar.setHidden(true);
      }
      
  render() {
    return (
    
      <View style={{justifyContent: 'center', flex: 1}}>
        <ImageBackground
        style={styles.container}
        source={require('../image/gameOver.png')}
        imageStyle={{ resizeMode: 'cover' }}
      >

      
      
      <Button 
        title = "Start"
        onPress = {()=>this.props.navigation.navigate("Game")}
       
      />
        
      </ImageBackground>
      </View>
      
    );
  }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#41f480",
      justifyContent: 'center',
      
    }
})