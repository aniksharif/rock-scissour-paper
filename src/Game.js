import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image, Alert, ImageBackground, StatusBar,Modal,Dimensions } from "react-native";




console.disableYellowBox = true;
const reply=''
const ques=''
const rockImg  = require('../image/rock.png')
const paperImg = require('../image/paper.png')
const scisrImg = require('../image/scissor.png')
const you=0
const cpu=0
const win=require('../image/win.png')
const lost=require('../image/lose.png')

export default class Game extends React.Component {

constructor(props){
  super(props);
  this.state={
    answer:'',
    result:'',
    blankBox: require('../image/select.jpg'),
    modal:false
  }
}

componentDidMount() {
  StatusBar.setHidden(true);
}


result(reply){
  console.log('reply:',reply)
  if(ques===reply){
    this.setState({answer:reply})
  }
  else if(ques===rockImg && reply===paperImg )
  {
    this.setState({answer:reply})
    if((cpu===4 || you===4) && you<cpu){
      this.setState({result:'Lost',modal:true})
    }
   else{
    ++cpu
   }
    
    // cpu===5 || you===5?you=cpu=0 && this.setState({modal:true}):cpu++;
  }
  else if(ques===paperImg && reply===scisrImg )
  {
    this.setState({answer:reply})
    if((cpu===4 || you===4) && you<cpu){
      this.setState({result:'Lost',modal:true})
    }
   else{
    ++cpu
   }
  }
  else if(ques===scisrImg && reply===rockImg )
  {
    this.setState({answer:reply})
    if((cpu===4 || you===4) && you<cpu){
      this.setState({result:'Lost',modal:true})
    }
   else{
     ++cpu
   }
  }
  else{
    // this.setState({result:'Win',answer:reply})
    this.setState({answer:reply})
    if((cpu===4 || you===4 ) && cpu<you ){
      this.setState({result:'Win',modal:true})
    }
   else{
     ++you
   }
  
}
}



tactic(){
  var ans=Math.random()*10;


if(ans>6){
  reply=rockImg
  
  this.result(reply)

}
else if(ans>3 && ans<6)
{
  reply=paperImg
  
 
  this.result(reply)
 
}
else{
  reply=scisrImg
  

  this.result(reply)
}

}

congrats=()=>{
  
  return(
<Modal
animationType={'slide'}
visible={this.state.modal}
transparent={false}
onRequestClose={()=>this.setState({modal:false})}
>

  <View style={{flex:1,height:Dimensions.get('window').height, backgroundColor: 'yellow'}}>
  <ImageBackground
  style={styles.container}
  source={require('../image/gameOver.png')}
  imageStyle={{ resizeMode: 'cover' }}>
  <Image
        style={{ width: '80%',alignSelf:'center',bottom:'10%'}}
        source={this.state.result==='Win'?win:lost}
       resizeMode={'contain'}
      >
      </Image>
      <TouchableOpacity style={{alignSelf:'center',bottom:'20%'}} onPress={()=>{you=0;cpu=0;this.setState({modal:false,answer:'',blankBox:require('../image/select.jpg')})}}> 
      <Image
      source={require('../image/restartBtn.png')} 
      resizeMode={'contain'}>
      
        </Image>
        </TouchableOpacity>
      </ImageBackground>
      </View>
  </Modal>
  );
}




  render() {
    return (
      
      <View style={styles.container}>
      
      <View style={{flex:1,}}>
      
      <Text style={{ justifyContent: 'center', marginLeft: '15%',  fontSize: 30, fontWeight: 'bold', height: 60,marginTop:this.state.modal===true?20:0 }}> 
          Touch to play Game 
          </Text>
          <ImageBackground
        style={styles.container}
        source={require('../image/game.gif')}
        imageStyle={{ resizeMode: 'cover' }}
      >
        <Text style={styles.player}>
          You ⟾
        </Text>
        <Text style={styles.Cpu}>
          Cpu ⟾
        </Text>


        <View style={styles.left}>
        
          <TouchableOpacity onPress={()=>{ques=rockImg; this.tactic();  this.setState({blankBox:rockImg}) }}>
              <Image
              style={styles.image}
              resizeMode='contain'
              source={require('../image/rock.png')}
               />
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ques=paperImg; this.tactic(); this.setState({blankBox:paperImg}) }}>
          <Image
              style={styles.image}
              resizeMode='contain'
              source={require('../image/paper.png')}
               />
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{ques=scisrImg; this.tactic(); this.setState({blankBox:scisrImg}) }}>
              <Image
              style={styles.image}
              resizeMode='contain'
              source={require('../image/scissor.png')}
               />
          </TouchableOpacity>
          
        </View>
        
        
        <View style={styles.right}>
         <View style={{flex:1, marginBottom: '40%', paddingBottom: 10}}>
          <Image
          style={styles.image}
          resizeMode='contain'
          source={this.state.blankBox}
          />

          <Image
          style={styles.image}
          resizeMode='contain'
          source={this.state.answer===''?this.state.blankBox:this.state.answer}
          />
        </View>
        </View>
        </ImageBackground>
      </View>
      <View style={{justifyContent:'space-between',flexDirection:'row', backgroundColor:'yellow',  borderColor: '#35443b',
    borderWidth: 10,
    }}>
        <Text style={{fontSize: 30, fontWeight: 'bold',fontWeight: 'bold',color: 'black',textShadowColor: 'white',textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10}}>{'You:'+you}</Text>
        <Text style={{fontSize: 30, fontWeight: 'bold',fontWeight: 'bold',color: 'black',textShadowColor: 'white',textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10 }}>{'Cpu:'+cpu}</Text>
        
        </View>
     
    
    {this.congrats()}
    </View>
    
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "yellow",
    
  },
  left: {
    paddingLeft: 5, 
    flex: 1,
    width: '50%',
    height: '100%'
  },
  right: {
    width: '50%',
    
    flex: 1,
    
    marginLeft: '70%',
    
   
  },
  image: {
    backgroundColor: 'yellow',
    height: '75%',
    width: '50%',
    borderColor: '#35443b',
    borderWidth: 10,
    
  },
  player: {
    fontSize: 30,
    marginLeft: '40%',
    flexDirection: 'row',
    overflow: 'hidden',
    top: '80%',
    fontWeight: 'bold',
    color: 'black',
    textShadowColor: 'white',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10
  },
  Cpu: {
    fontSize: 30,
    marginLeft: '40%',
    fontWeight: 'bold',
    overflow: 'hidden',
    top: '75%',
    fontWeight: 'bold',
    color: 'black',
    textShadowColor: 'white',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10
  }
});
