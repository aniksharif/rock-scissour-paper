import React, { Component } from 'react';
import {StyleSheet, ImageBackground, View, StatusBar, Button,TouchableOpacity,Text,Dimensions} from 'react-native';

console.disableYellowBox = true;



export default class Home extends Component {
    componentDidMount() {
        StatusBar.setHidden(true);
      }
      
  render() {
    return (
    
      <View style={{ flex: 1,justifyContent: 'center',alignItems:'center'}}>
        <ImageBackground
        style={styles.container}
        source={require('../image/home.gif')}
        imageStyle={{ resizeMode: 'contain' }}
      >

      
      <View  style={{overflow: 'hidden', top: '75%', width: '40%',alignSelf:'center' }}>
   
      <TouchableOpacity style={{backgroundColor:'blue',padding:10,}} onPress = {()=>this.props.navigation.navigate("Game")}>
          <Text style={{textAlign:'center',fontSize:18,color:'white'}}>{"Start"}</Text>
      </TouchableOpacity>
    </View>

      </ImageBackground>
      </View>
      
    );
  }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#41f480",
      width:Dimensions.get('window').width

      
    }
})