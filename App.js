import { StackNavigator } from 'react-navigation';
console.disableYellowBox = true;
import Home from './src/Home';
import Game from './src/Game';

const Navigation = StackNavigator({
  Home:{
    screen:Home, navigationOptions: { header: null }
  },
  Game:{
    screen:Game, navigationOptions: { header: null }
  },
})

export default Navigation;